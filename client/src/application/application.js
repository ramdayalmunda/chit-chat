import Sidebar from "./sidebar/sidebar.js"
import Header from "./header/header.js"
import ChatArea from "./chat-area/chat-area.js"
import ApplicationTemplate from "./applicataion-template.js"
import Authentication from "./authentication/authentication.js"
import { getLocalStore } from "../common/local-storage.js"
import api from "../common/api.js"


const { onMounted, ref, onBeforeMount } = Vue;

export default {
    components: {
        Sidebar,
        Header,
        ChatArea,
        Authentication,
        ApplicationTemplate,
    },
    setup() {
        let socket = ref(null)
        let chatComponent = ref(null)
        let userData = ref({
            loggedIn: false,
            _id: "",
            name: "",
        })
        let targetList = ref([])
        let userList = ref([])
        let selectedTarget = ref(null)

        async function loginInAgain() {
            let userDetails = getLocalStore('userDetails')
            if (userDetails?.name && userDetails?.password) {
                userData.value.loggedIn = true
                userData.value._id = userDetails._id
                userData.value.name = userDetails.name
                userData.value.password = userDetails.password
                enableSocket(true)
                await getTargetLists()
            } else {
                userData.value.loggedIn = false
                userData.value.name = ""
                userData.value.password = ""
            }
        }

        function enableSocket(enable){
            if (enable){
                socket.value = io(`http://localhost:3050?userId=${userData?.value?._id}`)
                socket.value.on('message', (messageData)=>{
                    if ([messageData.from, messageData.to].includes(selectedTarget.value._id))
                    chatComponent.value.receiveMessage(messageData)
                })
            }
            else if (socket?.value) socket.value.disconnect()
        }

        async function getTargetLists() {
            try {
                let response = await api.get('/target-list', { params: { userId: userData.value._id } })
                if (response?.data?.length) {
                    targetList.value = response.data
                }
            } catch (err) {
                console.log(err)
            }
        }

        function changeTarget(newTargetObj) {
            selectedTarget.value = { ...JSON.parse(JSON.stringify(newTargetObj)) }
        }

        onBeforeMount(async function () {
            await loginInAgain()
        })

        return {
            userData,
            targetList,
            userList,
            selectedTarget,
            socket,
            chatComponent,

            changeTarget,
            enableSocket,
        }
    },
    template: ApplicationTemplate
}