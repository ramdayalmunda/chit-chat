const appTemplate = /*html*/`
<template v-if="userData.loggedIn">
    <Header
        :userData="userData"
        @enable-socket="enableSocket"
    />

    <Sidebar
        :targetList="targetList"
        :userList="userList"
        @emit-target="changeTarget"
    />
    
    <ChatArea
        ref="chatComponent"
        :socket="socket"
        :userData="userData"
        :key="selectedTarget?._id"
        :selectedTarget="selectedTarget"
    />
</template>
<template v-else>
    <Authentication />
</template>
`

export default appTemplate