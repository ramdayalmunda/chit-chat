import api from "../../common/api.js";
import { setLocalStore } from "../../common/local-storage.js";
import authenticationTemplate from "./authentication-template.js"
const{ ref, onMounted } = Vue;
export default {
    setup(){
        const userData = ref({
            _id: "",
            name: "",
            password: "",
        })
        async function login(){
            console.log('loggin in',userData.value)
            // som authentication function
            try{
                let response = await api.post('/login', userData.value)
                if (response.data.success){
                    setLocalStore( response.data.userData, 'userDetails' )
                    location.reload()
                }else alert(response.data.message)
            }catch(err){
                console.log(err)
            }
        }
        return {
            userData,
            login,
        }
    },
    template: authenticationTemplate
}