const messageDao = require('../dao/message.js')
const userDao = require('../dao/user.js')

async function sendMessage( req, res ){
    try{
        let messageData = {
            from: req.body.from,
            to: req.body.to,
            groupId: req.body.groupId,
            content: req.body.content,
        }
        const newMessageData = await messageDao.postMessage(messageData)
        res.json(newMessageData)
    }catch(err){
        console.log(err)
        res.json([])
    }
}

async function getMessageList( req, res ){
    try{
        let messageList = await messageDao.getMessageListByGroupId(req.query.userId, req.query.targetId)
        res.json(messageList)
    }catch(err){
        console.log(err)
        res.json([])
    }
}

async function getLastMessageList ( req, res ){
    try{
        let userList = await userDao.getUserList(req.query.userId)
        res.json(userList)
    }catch(err){
        console.log(err)
        res.json([])
    }
}

module.exports = {
    sendMessage,
    getMessageList,
    getLastMessageList,
}