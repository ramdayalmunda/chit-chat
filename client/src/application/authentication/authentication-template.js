const authenticationTemplate = /*html*/`
    <div class="container">
        <h1>Login</h1>
        <form @submit.prevent="login" >
            <input id="user-name" v-model="userData.name" placeholder="user name">
            <input id="password" v-model="userData.password" placeholder="password">
            <button>Login</button>
        </form>
    </div>

`

export default authenticationTemplate