import sidebarTemplate from "./sidebar-template.js";

const { ref, onMounted, emit} = Vue;
export default {
    props: {
        targetList: { type: Array },
        userList: { type: Array },
    },
    setup( props, { emit } ){
        function changeTarget(newTargetObj){
            emit('emit-target', newTargetObj)
        }
        onMounted(async function(){
            // console.log('Sidebar mounted', props.targetList)
        })
        return {
            changeTarget,
        }
    },
    template: sidebarTemplate
}