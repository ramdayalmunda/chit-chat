import api from "../../common/api.js";
import chatAreaTemplate from "./chat-area-template.js"
const { ref, onMounted } = Vue;
export default {
    props: {
        userData: { type: Object },
        selectedTarget: { type: Object },
        socket: { type: Object },
    },
    setup(props) {
        let messageObj = ref({
            content: "",
            from: props.userData._id,
            to: props?.selectedTarget?._id,
        })
        let messageList = ref([])

        async function send() {
            const messageData = {
                content: messageObj.value.content,
                from: props.userData._id,
                to: props.selectedTarget._id,
            }
            // messageList.value.push(messageData)
            try {
                let response = await api.post('/message', messageData);
                props.socket.emit('message', response.data)
            } catch (err) {
                console.log(err)
            }
            messageObj.value.content = ""
        }

        async function getMessages() {
            try {
                if (!props?.selectedTarget?._id) return
                let response = await api.get('/message',
                    { params: { targetId: props.selectedTarget._id, userId: props.userData._id } }
                )
                messageList.value = response.data
            } catch (err) {
                console.log('error while fetching messages')
            }
        }

        function scrollToMessage(messageId) {
            setTimeout(()=>{
                const messageElem = document.getElementById(messageId)
                if (messageElem) messageElem.scrollIntoView({ behavior: "smooth", block: "end", inline: "nearest" })
            } )
        }

        async function receiveMessage(data) {
            messageList.value.push(data)
            scrollToMessage(data._id)
        }

        onMounted(async function () {
            await getMessages()
        })

        return {
            messageObj,
            messageList,

            send,
            receiveMessage,
        }
    },
    template: chatAreaTemplate
}