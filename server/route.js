const express = require('express')
const authenticationController = require('./controller/authentication.js')
const router = express.Router()
const messageController = require('./controller/message.js')
const userController = require('./controller/user.js')

router.post('/login', authenticationController.login)

router.get('/target-list', userController.getUserList)
router.get('/message', messageController.getMessageList)
router.post('/message', messageController.sendMessage)
router.get('/user-list', userController.getUserList)


module.exports = router