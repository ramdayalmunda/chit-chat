import { deleteLocalStore } from "../../common/local-storage.js"
import headerTemplate from "./header-template.js"

const { ref, onMounted, emit} = Vue
export default {
    props: {
        userData: { type: Object }
    },
    setup(props, { emit }){
        function logout(){
            deleteLocalStore('userDetails')
            emit('enable-socket', false)
            location.reload()
        }
        onMounted( function(){
            // console.log('header mounted', props.userData)
        } )
        return {
            logout
        }
    },
    template: headerTemplate
}