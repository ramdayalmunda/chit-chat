const mongoose = require('mongoose')
const message = require('./message.js')
const user = require('./user.js')

const model = {
    [message.collectionName]: mongoose.model(message.collectionName, message.collectionSchema),
    [user.collectionName]: mongoose.model(user.collectionName, user.collectionSchema),
}

module.exports = model