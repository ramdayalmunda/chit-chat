const userDao = require('../dao/user.js')

async function getUserList ( req, res ){
    try{
        let userList = await userDao.getUserList(req.query.userId)
        res.json(userList)
    }catch(err){
        console.log(err)
        res.json({})
    }
}

module.exports = {
    getUserList,
}