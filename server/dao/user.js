const mongoose = require("mongoose")
const model = require('../model/index.js')

async function getUserList(userId){
    return await model.user.aggregate([
        { $match: {
            _id: { $ne: mongoose.Types.ObjectId(userId) }
        } },
        { $project: {
            _id: 1,
            name: 1,
            fullName: 1,
            isGroup: 1,
            count: { $toInt: '0' },
            content: "",
        } }
    ])
}

async function getUserByName(name){
    return await model.user.findOne( { name: name } )
}

module.exports = {
    getUserList,
    getUserByName,
}