global.CONFIG = require('./config/config.js')
const express = require('express')
const PORT = 3030;
const app = express()
const path = require('path')

const { createProxyMiddleware } = require('http-proxy-middleware')

// const morgan = require('morgan') // dev
// app.use(morgan('dev')) // dev

app.use((request, response, next) => {
    response.header("Access-Control-Allow-Origin", "*");
    response.header(
        "Access-Control-Allow-Methods",
        "GET,HEAD,OPTIONS,POST,PUT,DELETE"
    );
    response.header(
        "Access-control-Allow-Headers",
        "Acess-Control-Allow-Headers,Origin,X-Requested-With,Content-type,Accept,Authorization,refreshToken"
    );
    next();
})

// back end server proxy
const backendServerUrl = `http://127.0.0.1:${global.CONFIG.SERVER_PORT}`;
app.use('/api', createProxyMiddleware({
    target: backendServerUrl,
    changeOrigin: true,
    pathRewrite: {
        [`^/api`]: '',
    },
}));

//socket server
const socketServerUrl = `http://127.0.0.1:${global.CONFIG.SOCKET_PORT}`;
app.use('/socket', createProxyMiddleware({
    target: socketServerUrl,
    changeOrigin: true,
    pathRewrite: {
        [`^/socket`]: '',
    }
}))

app.use(express.static(path.join(__dirname, '../client/')))


app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '../client/index.html'))
})


app.listen(global.CONFIG.CLIENT_PORT, () => { console.log(`listening on http://127.0.0.1:${global.CONFIG.CLIENT_PORT}`) })