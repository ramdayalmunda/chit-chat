function setLocalStore(data={}, name="") {
    if ( !name ) return {}
    let storeData = localStorage.getItem(name)
    if (storeData) storeData = JSON.parse(storeData)
    else storeData = {}
    
    storeData = {
        ...storeData,
        ...data
    }
    localStorage.setItem( name, JSON.stringify(storeData) )
    return storeData
}

function deleteLocalStore(  name){
    return localStorage.removeItem(name)
}

function getLocalStore ( name ){
    if ( !name) return {}
    let storeData = localStorage.getItem(name)
    if (storeData) return JSON.parse(storeData)
    else return {}
}
export {
    setLocalStore,
    deleteLocalStore,
    getLocalStore,
}