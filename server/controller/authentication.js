const { getUserByName } = require("../dao/user")

async function login(req, res) {
    try {
        const responseObj = {
            success: false,
            message: "User Not found",
            userData: {}
        }
        let userData = await getUserByName(req.body.name)
        if (userData) {
            if (userData.password == req.body.password) {
                responseObj.success = true;
                responseObj.message = "Logged in successfully."
                responseObj.userData = {
                    _id: userData._id,
                    name: userData.name,
                    password: userData.password,
                }

            } else responseObj.message = "Invalid Credentials"
        }
        res.json(responseObj)
    } catch (err) {
        console.log(err)
        res.json(responseObj)
    }
}

module.exports = {
    login,
}