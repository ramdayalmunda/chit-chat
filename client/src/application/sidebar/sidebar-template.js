const sidebarTemplate = /*html*/`
<div class="container-fluid">
    <ul class="ul-no-bullet">
        <template v-for="target in targetList">
            <li @click="changeTarget(target)">
                <p>
                    <span>{{target.fullName}}</span>
                    <span class="badge bg-success" v-if="target?.count">{{target.count}}</span><br>
                    <span>{{target.content}}</span>
                </p>
            </li>
        </template>
    </ul>
</div>

`
export default sidebarTemplate