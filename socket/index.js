global.CONFIG = require('./config.js')
const express = require('express')
const app = express()
const httpServer = require('http').createServer(app)

const io = require('socket.io')(httpServer, { cors: { origin: "*" } })
app.use('/*', (req, res) => { res.status(404).json({ message: "Page not found" }) })
httpServer.listen(global.CONFIG.SOCKET_PORT, () => { console.log(`socket on http://127.0.0.1:${global.CONFIG.SOCKET_PORT}`) })

const users = {};
io.on('connection', (socket) => {
    let userId = socket.handshake.query.userId
    // console.log('socket connection created:', socket.id, userId)
    if ( users[userId] && users[userId].length ) users[userId].push( socket.id )
    else users[userId] = [ socket.id ]

    socket.on("message", (data) => {
        // console.log('message',data)
        // to senders
        if ( users[data.to] && users[data.to].length ){
            users[data.to].forEach( sid => {
                io.to(sid).emit( 'message', data )
            } )
        }
        //to recievers
        if ( users[data.from] && users[data.from].length ){
            users[data.from].forEach( sid => {
                io.to(sid).emit( 'message', data )
            } )
        }
        // socket.broadcast.emit('message', data) // to send to everyone except oneself
    })
    socket.on('disconnect', async () => {
        // console.log('socket connection removed:', socket.id, userId)
        // let get = await io.fetchSockets();
        // console.log('connected : ',  get[0].id)
        let socketIndex = users[userId].findIndex( item => item == socket.id )
        if ( typeof(socketIndex)=='number' && socketIndex>=0 ) users[userId].splice( socketIndex, 1 )
        if ( users && users[userId] && typeof(users[userId].length)=='number' && users[userId].length==0 ) delete users[userId]
    });
})