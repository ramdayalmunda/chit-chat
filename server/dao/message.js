const mongoose = require('mongoose')
const model = require('../model/index.js');

async function postMessage(messageData){
    const messageObj = await model.message(messageData)
    return await messageObj.save()
}

async function getMessageListByGroupId (targetId, userId){
    return await model.message.find({
        $or: [
            { from: targetId, to: userId },
            { from: userId, to: targetId },
        ]
    })
}

async function getLastMessageList ( userId ){
    return await model.message.aggregate([
        { $match: {
            $or: [
                { from: mongoose.Types.ObjectId(userId) },
                { to: mongoose.Types.ObjectId(userId) },
                { groupId: { $in: [ "1234", "3465235" ] } },
            ]
        } },
        { $group: {
            _id: "$groupId",
            groupId: { $first: "$groupId" },
            from: { $first: "$from" },
            to: { $first: "$to" },
            content: { $last: "$content" },
            count: { $sum: 1 },
            updatedAt: { $last: "$updatedAt" },
        } },
        { $sort: { updatedAt: -1 } },
        { $lookup: {
            from: "users",
            localField: "from",
            foreignField: "_id",
            as: "fromData"
        } },
        { $unwind: "$fromData" },
        { $lookup: {
            from: "users",
            localField: "to",
            foreignField: "_id",
            as: "toData"
        } },
        { $unwind: "$toData" },
    ])
}

module.exports = {
    getLastMessageList,
    getMessageListByGroupId,
    postMessage,
}