const mongoose = require('mongoose')

const collectionSchema = mongoose.Schema({
    from: mongoose.Types.ObjectId,
    to: mongoose.Types.ObjectId,
    content: String,
    edited: { type: Boolean, default: false },
    deleted: { type: Boolean, default: false },
}, { timestamps: true });

const collectionName = "message";

module.exports = {
    collectionSchema,
    collectionName,
}