require('../config/index.js')
const { faker } = require('@faker-js/faker')
const model = require('../model/index.js')
const modelGenerator = [
    {
        model: "user",
        clear: true,
        fakeData: false,
        fakerDataCount: 10,
        documents: [
            {
                fullName: "Ram Dayal Munda",
                name: "RedBlueGreen",
                email: "RedBlueGreen@getnada.com",
                password: "RedBlueGreen",
                isGroup: false,
            },
            {
                fullName: "Pankaj Mahto",
                name: "svmPankaj",
                email: "svmPankaj@getnada.com",
                password: "svmPankaj",
                isGroup: false,
            },
            {
                fullName: "Aditya Aman",
                name: "NorseEagle",
                email: "NorseEagle@getnada.com",
                password: "NorseEagle",
                isGroup: false,
            },
            {
                fullName: "Akshay Soy",
                name: "ChickenTikka",
                email: "ChickenTikka@getnada.com",
                password: "ChickenTikka",
                isGroup: false,
            }
        ]
    },
    // {
    //     model: "message",
    //     clear: false,
    //     fakeData: true,
    //     fakerDataCount: 100,
    //     documents: []
    // },
]

!async function () {
    console.log('generating random Data')
    for (let i = 0; i < modelGenerator.length; i++) {
        console.log('generating', modelGenerator[i].model)
        if (modelGenerator[i].clear) {
            await model[modelGenerator[i].model].deleteMany()
            console.log(modelGenerator[i].model, 'cleared!')
        }
        for (let j = 0; j < modelGenerator[i].documents.length; j++) {
            let doc = await model[modelGenerator[i].model](modelGenerator[i].documents[j])
            await doc.save()
            console.log(`${j + 1} ${modelGenerator[i].model} generated`)
        }

        if (modelGenerator[i].fakeData) {
            let docs = []
            if (modelGenerator[i].model == 'user' && modelGenerator[i].fakerDataCount) {
                for (let j = 0; j < modelGenerator[i].fakerDataCount; j++) {
                    let userData = {
                        fullName: faker.name.fullName(),
                        name: faker.internet.userName(),
                        password: faker.internet.password(),
                    }
                    userData.email = userData.name + '@getnada.com'
                    docs.push(userData)
                }
            }

            if (modelGenerator[i].model == 'message' && modelGenerator[i].fakerDataCount) {
                let userList = await model.user.find({}).lean()
                if (userList.length) {
                    for (let j = 0; j < modelGenerator[i].fakerDataCount; j++) {
                        let fromUser = faker.helpers.arrayElement(userList)
                        let toUser = faker.helpers.arrayElement(userList)
                        let groupId = fromUser._id.toString() < toUser._id.toString()
                        groupId = groupId ? (fromUser._id.toString() + "-" + toUser._id.toString()) : (toUser._id.toString() + "-" + fromUser._id.toString())
                        let messageData = {
                            groupId: groupId,
                            from: fromUser._id,
                            to: toUser._id,
                            content: faker.lorem.lines(1)
                        }
                        if (messageData.from != messageData.to) docs.push(messageData)
                    }
                }
            }

            for (let j = 0; j < docs.length; j++) {
                let doc = await model[modelGenerator[i].model](docs[j])
                await doc.save()
                console.log(`${j + 1} fake ${modelGenerator[i].model} generated`)
            }
        }

    }

    process.exit()
}()