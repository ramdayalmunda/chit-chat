const { CLIENT_PORT, SERVER_PORT, SOCKET_PORT } = require("./server/config/config.js")

module.exports = {
    apps: [
        {
            name: `CC client: ${CLIENT_PORT}`,
            script: "./server/static.js",
        },
        {
            name: `CC server: ${SERVER_PORT}`,
            script: "./server/index.js",
        },
        {
            name: `CC socket: ${SOCKET_PORT}`,
            script: "./socket/index.js",
        },
    ]
}
