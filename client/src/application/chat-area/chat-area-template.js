export default /*html*/`
<div class="container-fluid">
    <template v-if="selectedTarget?._id">
        <div class="h1">{{selectedTarget?.fullName}}<span class="h5 text-gray">{{selectedTarget?.name}}</span></div>
        <div class="chat-table" id="chat-table">
            <template v-if="messageList?.length" v-for="(message, m) in messageList">
                    <div :class="[userData._id==message.from?'align-hr':'align-hl']" :id="message._id">
                        <div class="talk-bubble tri"
                            :class="[(userData._id==message.from?'right-top bg-you':'left-top bg-target')]">
                            <div class="talktext">
                                <p>{{message.content}}</p>
                            </div>
                        </div>
                    </div>
            </template>
            <div v-else>
                <td class="align-hc text-gray">
                    <p>Send a message to start chatting</p>
                </td>
            </div>
        </div>
        <div class="input-group mb-3">
            <input type="text" class="form-control" placeholder="message" v-model="messageObj.content">
            <button class="btn btn-outline-primary" type="button" @click="send">Button</button>
        </div>
    </template>
    <template v-else>
        <div>Blank Area</div>
    </template>
</div>
`