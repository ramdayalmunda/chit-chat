const MDB = {
    url: "mongodb://127.0.0.1:27017",
    options: {
        useNewUrlParser: true,
        useUnifiedTopology: true
    },
    db: "chitChat"
}


const CLIENT_PORT = 3030;
const SOCKET_PORT = 3050;
const SERVER_PORT = 3090;
module.exports = {
    MDB,
    CLIENT_PORT,
    SOCKET_PORT,
    SERVER_PORT,
}