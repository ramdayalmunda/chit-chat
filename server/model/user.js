const mongoose = require('mongoose')

const collectionSchema = mongoose.Schema({
    fullName: String,
    name: { type: String, unique: true },
    isGroup: Boolean,
    email: String,
    password: String,
}, { timestamps: true });

const collectionName = "user";

module.exports = {
    collectionSchema,
    collectionName,
}