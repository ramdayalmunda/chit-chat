global.CONFIG = require('./config/config.js')

const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const router = require('./route.js')
require('./config/index.js')

app.use((request, response, next) => {
    response.header("Access-Control-Allow-Origin", "*");
    response.header(
        "Access-Control-Allow-Methods",
        "GET,HEAD,OPTIONS,POST,PUT,DELETE"
    );
    response.header(
        "Access-control-Allow-Headers",
        "Acess-Control-Allow-Headers,Origin,X-Requested-With,Content-type,Accept,Authorization,refreshToken"
    );
    next();
})

app.use(bodyParser.json())

app.use(router)

app.listen(global.CONFIG.SERVER_PORT, () => { console.log(`server on http://127.0.0.1:${global.CONFIG.SERVER_PORT}`) })